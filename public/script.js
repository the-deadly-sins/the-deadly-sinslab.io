const quotes = [
    /*
    [ "No, saying \"Spank me daddy, I've been a bad girl!\" will NOT rid you of your sins.", "Soumeh" ],
    [ "Forgive me father, for I have sinned.", "Soumeh" ],
    [ "God damn the sin.", "Soumeh" ],
    [ "The Sins 4.", "Admiral" ],
    [ "Comic Sins.", "Soumeh" ],
    [ "May your woes be many, and your days few.", "Potassium" ],
    [ "You'll be in luck if what's down there is better than what we have up here.", "Soumeh" ]
    */
    [ "The Sins 4.", "Anonymous" ],
]

const pages = ['main', 'sins', 'virtues', 'error', 'sinners', 'meme']

addEventListener('hashchange', (event) => { });
onhashchange = (event) => {
    var newPage = event.newURL.split('#')[1]
    var oldPage = event.oldURL.split('#')[1]
    selectPage(newPage, oldPage)
};

function selectPage(newPage, oldPage) {

    if (!newPage) var newPage = window.location.hash.split('#')[1]
    if (!newPage) var newPage = 'main'
    if (newPage && !pages.includes(newPage)) var newPage = 'error'
    if (oldPage && !pages.includes(oldPage)) var oldPage = 'error'

    if (oldPage && newPage != oldPage) {
        var a = document.querySelector(`a#${oldPage}`) || false
        if (a) a.classList.remove('selected')
        var page = document.querySelector(`page#${oldPage}`)
        if (page) page.classList.remove('selected')
    }

    var a = document.querySelector(`a#${newPage}`) || false
    if (a) a.classList.add('selected')
    var page = document.querySelector(`page#${newPage}`)
    if (page) page.classList.add('selected')
}

function changeQuote(parent) {
    const result = quotes[Math.floor(Math.random() * quotes.length)]
    const quote = result[0]
    const author = result[1]

    const b = parent.getElementsByTagName('blockquote')[0]
    const p = parent.getElementsByTagName('figcaption')[0]
    b.innerHTML = quote
    p.innerHTML = author
}

function rotateImages() {
    const galleries = document.getElementsByTagName('gallery')
    for (const gallery of galleries) {
        const images = gallery.getElementsByTagName('img')
        for (const image of images) {
            var degree = randomNumber(-9, 9)
            image.style.setProperty('--rotation', `${degree}deg`)
        }
    }
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

var localStorage = window.localStorage

function main() {
    const theme = localStorage.getItem('theme') || null
    if (theme == null) localStorage.setItem('theme', false)

}

function setTheme() {
    var theme = localStorage.getItem('theme') === 'true' || null
    if (theme == null) {
        localStorage.setItem('theme', false)
        var theme = false
    }
    if (theme) {
        document.querySelector(":root").classList.add('switched-theme')
    } else {
        document.querySelector(":root").classList.remove('switched-theme')
    }
}

function toggleTheme() {
    var theme = localStorage.getItem('theme') === 'true' || false
    if (theme) {
        document.querySelector(":root").classList.remove('switched-theme')
    } else {
        document.querySelector(":root").classList.add('switched-theme')
    }
    localStorage.setItem('theme', !theme)
}